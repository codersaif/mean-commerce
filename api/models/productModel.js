'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var ProductSchema = new Schema({
    name: {type: String,required:true},
    category: {
        type: Schema.Types.ObjectId,
        ref: 'Category',
        default: null
    },
    images: [{id: Schema.Types.ObjectId, alt: String, title: String, displayOrder:Number}],
    defaultImage: {type: Number, default:0},
    description: String,
    price: Number,
    inStock: Number,
    discount: {type: Number, default: 0},
    sku: {type:String, unique:true}
});
var Product = mongoose.model('Product', ProductSchema);

module.exports = Product;