var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
require('models/productModel');
var Product = mongoose.model('Product');
/***************************************************** Address ***********************/
var AddressSchema = new Schema({
    name: String,
    address: String,
    city: String,
    state: String,
    zip: String
}, {_id: false});
mongoose.model('Address', AddressSchema);

/***************************************************** Billing ***********************/
var BillingSchema = new Schema({
    cardType: {type: String, enum: ['Visa', 'MasterCard', 'Amex']},
    name: String,
    number: String,
    expireMonth: Number,
    expireYear: Number,
    address: [AddressSchema]
}, {_id: false});
mongoose.model("Billing", BillingSchema);

/***************************************************** Category ***********************/


/***************************************************** Product ***********************/


/***************************************************** Product Quantity ***********************/
var ProductQuantitySchema = new Schema({
    quantity: Number,
    product: [{type:Schema.Types.ObjectId, ref: Product}]
}, {_id: false});
mongoose.model('ProductQuantity', ProductQuantitySchema);

/***************************************************** Order ***********************/
var OrderSchema = new Schema({
    userId: String,
    items: [ProductQuantitySchema],
    shipping: [AddressSchema],
    billing: [BillingSchema],
    status: {type: String, default: "Pending"},
    timeStamp: {type: String, default: Date.now}
});
mongoose.model("Order", OrderSchema);

/***************************************************** Customer ***********************/
var CustomerSchema = new Schema({
    userId: {type: String, unique: true, required: true},
    shipping: [AddressSchema],
    billing: [BillingSchema],
    cart: [ProductQuantitySchema]
});
mongoose.model("Customer", CustomerSchema);


