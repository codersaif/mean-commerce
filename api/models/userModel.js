'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var UserSchema = new Schema({
    username: {type:String, unique:true},
    email: String,
    color:String,
    roles: [String],
    salt: String,
    hashed_password:String
});

var User = mongoose.model('User', UserSchema);

module.exports = User;