'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var ManufacturerSchema = new Schema({
    name: {type: String,required:true},
    logos: [Schema.Types.ObjectId],
    logoDefault: Schema.Types.ObjectId,
    info: String,
    linkExternal: [String]
});
var Manufacturer = mongoose.model('Manufacturer', ManufacturerSchema);

module.exports = Manufacturer;