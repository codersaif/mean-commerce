'use strict';

var express = require('express');
var router = express.Router();
var auth = require('auth');
var categoryController = require('controllers/categoryController');
var config = require('config');



router.get('/',categoryController.getCategories);
router.post('/', auth.admin, categoryController.addNewCategory);
router.delete('/:catId',auth.admin, categoryController.removeCategory);
router.put('/',auth.admin, categoryController.updateCategory);

module.exports = router;