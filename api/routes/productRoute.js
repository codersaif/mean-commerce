'use strict';

var express = require('express');
var router = express.Router();
var auth = require('auth');
var productController = require('controllers/productController');
var config = require('config');



router.get('/',productController.getProducts);
router.get('/:productId',productController.getProduct);
router.post('/', auth.admin, productController.addProduct);
router.delete('/:productId',auth.admin, productController.removeProduct);
router.put('/',auth.admin, productController.updateProduct);

router.get('/media/:mediaId',productController.getMedia);
router.post('/media/', productController.addMedia);
//router.delete('/media',productController.removeMedia);
module.exports = router;