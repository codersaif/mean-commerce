var express = require('express');
var router = express.Router();
var config = require('config');
var webDir = config.WEB_VIEW_DIR;
var webTheme = config.WEB_THEME;
var publicWeb = config.PUBLIC_WEB;

/* GET home page. */
router.get('/', function (req, res, next) {
    res.render(webDir + webTheme + 'index', {
        public: publicWeb,
        title: 'Express'
    });
});

module.exports = router;
