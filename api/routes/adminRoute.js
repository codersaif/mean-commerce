'use strict';

var express = require('express');
var router = express.Router();
var auth = require('auth');
var adminController = require('controllers/adminController');
var config = require('config');
var adminDir = config.ADMIN_VIEW_DIR;
var adminTheme = config.ADMIN_THEME;
var publicAdmin = config.PUBLIC_ADMIN;


/* GET admins listing. */ //auth.admin,
router.get('/', auth.admin,  function(req, res, next) {
    res.render(adminDir+adminTheme+'index',{
        public: publicAdmin
    });
});

router.get('/logout', function(req, res, next) {
    req.session.destroy(function(err) {
        res.redirect('/admin/login');
    });


});
router.post('/login', adminController.login);
router.get('/login', function(req, res){
    if(req.session.admin){
        console.log('redirecting to admin root')
        res.redirect('/admin');
    }else{
        console.log('redirecting to login page')
        res.render(adminDir+adminTheme+'login', {
            public: publicAdmin,
            msg:req.session.msg
        });
    }
});

router.get('/signup', function(req, res){
    if(req.session.admin){
        res.redirect('/admin');
    }
    res.render(adminDir+adminTheme+'signup', {
        public: publicAdmin,
        msg:req.session.msg,
        err: req.session.error
    });
});


router.post('/signup', adminController.signup);

router.put('/login', function(req, res){
    if(req.session.admin){
        console.log('redirecting to admin root')
        res.redirect('/admin');
    }else{
        console.log('rendering login page');
        req.session.destroy();
        res.status(404).json({});
    }
});
router.delete('/login', function(req, res){
    if(req.session.admin){
        console.log('redirecting to admin root')
        res.redirect('/admin');
    }else{
        req.session.destroy();
        res.status(404).json({});
    }
});


module.exports = router;
