var express = require('express');
module.exports = function (app) {
    var categories = require('controllers/categoryController');

    app.get('/admin', function (req, res) {
        res.render('admin.html');
    });

    app.get('/categories/get', categories.getCategories);
    //app.post('/categories/add', categories.addNewCategories);
    //app.post('/categories/remove', categories.removeCategories);
};
