'use strict';

var express = require('express');
var router = express.Router();
var auth = require('auth');
var manufacturerController = require('controllers/manufacturerController');
var config = require('config');


router.get('/',manufacturerController.getManufacturers);
router.post('/', auth.admin, manufacturerController.addNewManufacturer);
router.delete('/:mfId',auth.admin, manufacturerController.removeManufacturer);
router.put('/',auth.admin, manufacturerController.updateManufacturer);

module.exports = router;