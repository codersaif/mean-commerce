var mongoose = require('mongoose');
require('models/manufacturerModel');
var Manufacturer = mongoose.model('Manufacturer');

exports.getManufacturers = function (req, res) {
    Manufacturer.find({}).exec(function (err, manufacturers) {
        if (err) {
            res.json("manufacturers not found");
        } else {
            res.json(manufacturers);
        }
    });
}
exports.addNewManufacturer = function (req, res) {
    Manufacturer.find({}).exec(function (err, manufacturers) {
        if (err) {
            //res.json("Manufacturers not found");
        } else {
            var found = false;
            for (var i in manufacturers) {
                if (manufacturers[i].name == req.body.name) {
                    found = true;
                }
            }
            if (!found) {
                var manufacturer = new Manufacturer(
                    {
                        name: req.body.name,
                        parent: req.body.parent ? req.body.parent : null
                    });
                manufacturer.save(function (err, result) {
                    if (err) {
                        res.json("Manufacturer not saved");
                    } else {
                        res.json(result);
                    }
                });
            }
        }

    });
}

exports.removeManufacturer = function(req, res){
    Manufacturer.findOne({_id: req.params.mfId}).remove(function (err, result) {
        if (err) {
            res.json("Manufacturer not removed");
        } else {
            res.json(err);
        }
    });
}


exports.updateManufacturer = function(req, res){
    console.log('Manufacturer updating');
    var manufacturer = {
        name: req.body.name,
        parent: req.body.parent
    }
    Manufacturer.update({_id: req.body.id}, manufacturer, {upsert: false}, function(err){
        if (err) {
            res.json("Manufacturer not updated");
        } else {
            res.json(err);
        }
    });
}