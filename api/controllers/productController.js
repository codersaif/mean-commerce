var mongoose = require('mongoose');
require('models/productModel');
var Product = mongoose.model('Product');

var Q = require('q');
var mediaController = require('controllers/mediaController');

exports.getProduct = function (req, res) {
    console.log(req.params.productId);
    Product.findOne({_id: req.params.productId}).exec(function (err, product) {
        if (!product) {
            res.json(404, {msg: 'product Not Found'});
        } else {
            res.json(product);
        }
    });
};

exports.getProducts = function (req, res) {
    Product.find({}).populate('category').exec(function (err, products) {
        if (!products) {
            res.json(404, {msg: 'Products Not Found'});
        } else {
            res.json(products);
        }
    })
};


exports.addProduct = function (req, res) {
    var product = new Product({
        name: req.body.name,
        category: req.body.category,
        description: req.body.description,
        price: req.body.price,
        inStock: req.body.inStock,
        discount: req.body.discount,
        sku: req.body.sku,
        images: req.body.images,
        defaultImage: parseInt(req.body.defaultImage)
    });
    product.save(function (err, result) {
        if (err) {
            res.json(err);
        } else {
            res.json(result);
        }
    });
}

exports.removeProduct = function (req, res) {
    Product.findOne({_id: req.params.productId}).remove(function (err, result) {
        if (err) {
            res.json("Product not removed");
        } else {
            res.json(result);
        }
    });
}


exports.updateProduct = function (req, res) {
    console.log('Product updating');
    console.log(req.body);
    var product = {
        name: req.body.name,
        category: req.body.category,
        description: req.body.description,
        price: parseInt(req.body.price),
        inStock: parseInt(req.body.inStock),
        discount: parseInt(req.body.discount),
        sku: req.body.sku,
        images: req.body.images,
        defaultImage: parseInt(req.body.defaultImage)
    };
    Product.update({_id: req.body.id}, product, {upsert: true}, function (err) {
        if (err) {
            console.log(err);
            res.status(404).json(err);
        } else {
            res.json(200, {});
        }
    });
}


exports.addMedia = function (req, res) {
    mediaController.addMedia(req.files.file)
        .then(function (file) {
            return res.status(200).json(file);
        }).catch(function (error) {
            console.log(error);
            return res.status(500).json({error: error});
        })
        .done();

}

exports.getMedia = function (req, res) {
    /*var stream = mediaController.getMedia(req.params.mediaId);
     stream.pipe(res);
     return res.status(200);*/
    mediaController.getMedia(req.params.mediaId)
        .then(function (stream) {
            stream.pipe(res);
            return res.status(200);
        })
        .catch(function (error) {
            console.log(error);
            return res.status(500).json({error: error});
        })
        .done();

}

exports.removeMedia = function (req, res) {
    /*var stream = mediaController.getMedia(req.params.mediaId);
     stream.pipe(res);
     return res.status(200);*/
    mediaController.removeMedia(req.params.mediaId)
        .then(function (result) {
            return res.status(200).json({});
        })
        .catch(function (error) {
            console.log(error);
            return res.status(500).json({error: error});
        })
        .done();

}