'use strict';

var mongoose = require('mongoose'),
    Grid = require('gridfs-stream'),
    Q = require('q');

Grid.mongo  = mongoose.mongo;
var gfs = new Grid(mongoose.connection.db);

exports.addMedia = function(file){
    var deferred = Q.defer();

    var writeStream = gfs.createWriteStream({
        filename: file.name,
        mode: 'w',
        content_type: file.mimetype
    });

    writeStream.on('close', function(f) {
        deferred.resolve(f);
    });

    writeStream.on('error', function(error){
        deferred.reject(error);
    });

    //Write buffer data
    writeStream.write(file.buffer);
    writeStream.end();

    return deferred.promise;
};



exports.getMedia= function(fileId){
    var deferred = Q.defer();
    gfs.findOne({ _id: fileId}, function (err, file) {
        if(!file){
            deferred.reject(err);
        }else{
            var stream = gfs.createReadStream({
                _id: fileId
            });
            return deferred.resolve(stream);
        }

    });
    return deferred.promise;

};


exports.removeMedia = function(fileId){
    var deferred = Q.defer();

    gfs.remove({
        _id: fileId
    }, function(error){
        if(error){
            return deferred.reject(error);
        }
        return deferred.resolve();
    });

    return deferred.promise;
};