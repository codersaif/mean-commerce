'use strict';

var crypto = require('crypto');


var mongoose = require('mongoose'),
    User = require('models/userModel');


function hashPW(pwd){
    return crypto.createHash('sha256').update(pwd).digest('base64').toString();
}

function checkAdmin(user){
    for(var i in user.roles){
        if(user.roles[i] == 'admin'){
            return true;
        }
    }
    return false;
}

exports.signup = function(req, res){

    console.log('signing up...');
    var user = new User({
        username: req.body.username
    });
    user.set('hashed_password', hashPW(req.body.password));
    user.set('email', req.body.email);
    user.set('roles', ['admin', 'user']);
    console.log('going to save user');
    console.log(user);

    user.save(function(err){
        if(err){
            req.session.error = err.errmsg;
            console.log(req.session.error);
            res.redirect('/admin/signup');
        }else{
            req.session.admin = user.id;
            req.session.username = user.username;
            req.session.msg = 'Authenticated as ' + user.username;
            console.log('redirecting to admin');
            res.redirect('/admin');
        }
    });
};


exports.login = function(req, res){

    User.findOne({username:req.body.username})
        .exec(function(err, user){
            if(!user){
                err = 'User Not Found.';
                console.log(err);
            }else if(user.hashed_password === hashPW(req.body.password.toString())){
                if(!checkAdmin(user)){
                    err = 'Not an Admin';
                    console.log(err);
                }else{
                    console.log('else block');
                    req.session.regenerate(function(){
                        req.session.role = user.roles;
                        req.session.user = user.id;
                        req.session.admin = true;
                        req.session.username = user.username;
                        req.session.msg = 'Authenticated as ' + user.username;
                        res.redirect('/admin')
                    });
                }
            }else{
                err = 'Authetication failed.';
                console.log(err);
            }
            if(err){
                console.log('admin login');
                req.session.regenerate(function(){
                    req.session.msg = err;
                    res.redirect('/admin/login');
                });
            }
        });
};