var mongoose = require('mongoose');
require('models/cart_model');
var Customer = mongoose.model('Customer');
var Address = mongoose.model('Address');
var Billing = mongoose.model('Billing');

exports.getCustomer = function (req, res) {
    Customer.findOne({userId: 'customerA'})
        .exec(function (err, customer) {
            if (!customer) {
                res.json(404, {msg: 'Customer Not Found'});
            } else {
                res.json(customer);
            }
        });
};

exports.updateShipping = function (req, res) {
    var newShipping = new Address(req.body.updatedShipping);
    Customer.update({userId: 'customerA'},
        {
            $set: {
                shipping: [newShipping.toObject()]
            }
        }
    ).exec(function (err, results) {
            if (err || results < 1) {
                res.json(404, {msg: 'Failed to update Shipping.'});
            } else {
                res.json({msg: "Customer Shipping Updated"});
            }
        });
};

exports.updateBilling = function (req, res) {
    var newBilling = new Billing(req.body.updatedBilling);
    Customer.update({userId: 'customerA'},
        {
            $set: {
                shipping: [newBilling.toObject()]
            }
        }
    ).exec(function (err, results) {
            if (err || results < 1) {
                res.json(404, {msg: "Failed to update Billing."});
            } else {
                res.json({msg: "Customer Billing updated"})
            }
        })
};

exports.updateCart = function (req, res) {
    Customer.update({userId: 'customerA'},
        {
            $set: {
                cart: req.body.updatedCart
            }
        }
    ).exec(function (err, results) {
            if (err || results < 1) {
                res.json(404, {msg: 'Failed to update Cart.'});
            } else {
                res.json({msg: "Customer Cart Updated"});
            }
        })
};
