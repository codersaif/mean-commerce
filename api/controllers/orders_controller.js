var mongoose = require('mongoose');
var Customer = mongoose.model('Customer');
var Order = mongoose.model('Order');
var Address = mongoose.model('Address');
var Billing = mongoose.model('Billing');


exports.getOrder = function (req, res) {
    Order.findOne({_id: req.query.orderId}).exec(function (err, order) {
        if (!order) {
            res.json(404, {msg: 'Order Not Found'});
        } else {
            res.json(order);
        }
    });
};

exports.getOrders = function (req, res) {
    Order.find({userId: 'customerA'})
        .exec(function (err, orders) {
            if (!orders) {
                res.json(404, {msg: 'Orders Not Found'});
            } else {
                res.json(orders);
            }
        })
};

exports.addOrder = function (req, res) {
    var orderShipping = new Address(req.body.updatedShipping);
    var orderBilling = new Billing(req.body.updatedBilling);
    var orderItems = req.body.orderItems;
    var newOrder = new Order({
        userId: 'customerA',
        items: orderItems,
        shipping: orderShipping,
        billing: orderBilling
    });
    newOrder.save(function (err, results) {
        if (err) {
            res.json(500, "Failed to save Order.");
        } else {
            Customer.update({userId: 'customerA'},
                {$set: {cart: []}})
                .exec(function (err, results) {
                    if (err || results < 1) {
                        res.json(404, {msg: 'Failed to update Cart.'});
                    } else {
                        res.json({msg: "Order Saved"});
                    }
                });
        };
    });
};
