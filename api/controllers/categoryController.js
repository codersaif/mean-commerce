var mongoose = require('mongoose');
require('models/categoryModel');
var Category = mongoose.model('Category');

exports.getCategories = function (req, res) {
    Category.find({}).exec(function (err, categories) {
        if (err) {
            res.json("Categories not found");
        } else {
            res.json(categories);
        }
    });
}
exports.addNewCategory = function (req, res) {
    Category.find({}).exec(function (err, categories) {
        if (err) {
            //res.json("Categories not found");
        } else {
            var found = false;
            for (var i in categories) {
                if (categories[i].name == req.body.name) {
                    found = true;
                }
            }
            if (!found) {
                console.log(req.body.name, req. body.parent);
                var category = new Category(
                    {
                        name: req.body.name,
                        parent: req.body.parent ? req.body.parent : null
                    });
                category.save(function (err, result) {
                    if (err) {
                        res.json("Category not saved");
                    } else {
                        res.json(result);
                    }
                });
            }
        }

    });
}

exports.removeCategory = function(req, res){
    Category.findOne({_id: req.params.catId}).remove(function (err, result) {
        if (err) {
            res.json("Category not removed");
        } else {
            res.json(err);
        }
    });
}


exports.updateCategory = function(req, res){

    console.log('category updating');
    var category = {
        name: req.body.name,
        parent: req.body.parent
    }
    Category.update({_id: req.body.id}, category, {upsert: true}, function(err){
        if (err) {
            res.json("Category not updated");
        } else {
            res.json(err);
        }
    });
}

/*
exports.removeCategories = function (req, res) {
    console.log(req.body.name + " " + req.body.parent);
    Category.find({name: req.body.name, parent: req.body.parent}).remove(function (err, result) {
        if (err) {
            res.json("Category not removed");
        } else {
            res.json(result);
        }
    });
}*/
