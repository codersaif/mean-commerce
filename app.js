'use strict';
require('app-module-path').addPath(__dirname + '/api');
require('app-module-path').addPath(__dirname + '/app_modules');

var express = require('express');
var path = require('path');
var mongoose = require('mongoose');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');
var multer = require('multer');
var config = require('config');

var conn = mongoose.connect('mongodb://localhost/myshop');
var swig = require('swig');

var routes = require('routes/indexRoute');
var admin = require('routes/adminRoute');
var user = require('routes/userRoute');
var category = require('routes/categoryRoute');
var manufacturer = require('routes/manufacturerRoute');
var product = require('routes/productRoute');

var app = express();


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.engine('html', swig.renderFile);
app.set('view engine', 'html');
app.set('views', __dirname + '/views');

//app.set('view cache', false);
// To disable Swig's cache, do the following:
swig.setDefaults({ cache: false });

// uncomment after placing your favicon in /public
app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());
app.use(session({
    secret: 'My big secret session key',
    cookie: {maxAge: 60 * 60 * 1000},
    resave: false,
    saveUninitialized: true/*,
     store: new mongoStore({
     db: mongoose.connection.db,
     collection: 'sessions'
     })*/
}));
app.use(multer({
    inMemory: true
}));


app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'bower_components')));

app.use('/', routes);
app.use('/admin', admin);
app.use('/user', user);
app.use('/api/categories', category);
app.use('/api/manufacturers', manufacturer);
app.use('/api/products', product);

require('routes/cartRoutes')(app);
require('routes/cartAdminRoutes')(app);


// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        var webDir = config.WEB_VIEW_DIR;
        var webTheme = config.WEB_THEME;
        res.render(webDir + webTheme + 'error', {
            public: config.PUBLIC_WEB,
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render(webDir + webTheme + 'error', {
        public: config.PUBLIC_WEB,
        message: err.message,
        error: {}
    });
});


module.exports = app;
