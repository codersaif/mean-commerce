'use strict';

angular.module('appAdmin')
    .provider('configuration', function () {
        //var THEME_DIR = '/admin/default/';
        //this.VIEWS_DIR = this.THEME_DIR + 'views/';
        return {
            $get: function () {
                return {
                    THEME_DIR: '/admin/default/',
                    VIEWS_DIR: '/admin/default/views/'
                }
            }
        }
    }
);

/*
app.provider("game", function () {
    var type;
    return {
        setType: function (value) {
            type = value;
        },
        $get: function () {
            return {
                title: type + "Craft"
            };
        }
    };
});

app.config(function (gameProvider) {
    gameProvider.setType("War");
});

app.controller("AppCtrl", function ($scope, game) {
    $scope.title = game.title;
});
*/