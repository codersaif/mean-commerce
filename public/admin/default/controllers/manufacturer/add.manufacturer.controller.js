'use strict';

angular.module('appAdmin')
    .controller('addManufacturerController', ['$scope', '$http', '$state',
        function ($scope, $http, $state) {
            $scope.cat = {};
            $scope.categories = [];

            $scope.refreshCategories = function () {

                $http.get('/api/categories').
                    success(function (data, status, headers, config) {
                        $scope.categories = [{id: '', parent: null, name: 'None', text: 'None'}];
                        for (var i in data) {
                            $scope.categories.push({
                                id: data[i]._id,
                                text: $scope.getRootCategory(data[i].name, data),
                                name: data[i].name,
                                parent: data[i].parent
                            });
                        }
                        $scope.cat.parent = '';
                    })
                    .error(function (data, status, headers, config) {
                    });
            }();

            $scope.save = function (cont) {
                console.log($scope.cat);
               $http.post('/api/categories', {name: $scope.cat.name, parent: $scope.cat.parent?$scope.cat.parent: null })
                    .success(function (data, status, headers, config) {
                        if (cont) {
                            $scope.cat.name = '';
                            $scope.cat.parent = '';
                        }else{
                            $state.go('Category.List');
                        }
                    })
                    .error(function (data, status, headers, config) {
                    });
            }

            $scope.goBack = function () {
                $state.go('Category.List');
            }

            $scope.getRootCategory = function (categoryName, data) {
                function getParent(name) {
                    for (var i in data) {
                        if (data[i].name == name) {
                            if (!data[i].parent) {
                                return data[i].name;
                            } else {
                                return getParent(data[i].parent) + ' >> ' + name;
                            }
                        }else if(data[i]._id == name){
                            if (!data[i].parent) {
                                return data[i].name;
                            } else {
                                return getParent(data[i].parent) + ' >> ' + data[i].name;
                            }
                        }
                    }
                }
                return getParent(categoryName);
            };


        }]);
