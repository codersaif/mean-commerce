'use strict';


angular.module('appAdmin').directive('categoryTree', ['$http', '$state',
    function ($http, $state) {
        return {
            replace: true,
            template: '<div id="category_jstree_div"></div>',
            link: function (scope, element, attrs) {

                var catTree = [];

                $http.get('/api/categories').
                    success(function (data, status, headers, config) {
                        for (var i in data) {
                            var item = {};
                            item.id = data[i]._id;
                            item.parent = '#';
                            if (data[i].parent) {
                                item.parent = data[i].parent;
                            }

                            item.text = data[i].name;
                            catTree.push(item);
                        }

                        $('#category_jstree_div').jstree({
                            'core': {
                                'data': catTree,
                                'dblclick_toggle': false
                            }
                        })
                            .
                            on('activate_node.jstree', function (e, data) {
                                $state.go('Category.Edit', {catId: data.node.id});
                            });
                    }).
                    error(function (data, status, headers, config) {

                    });
            }

        };
    }
]);


