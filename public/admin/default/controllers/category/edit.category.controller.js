'use strict';

angular.module('appAdmin')
    .controller('editCategoryController', ['$scope', '$http', '$state', '$stateParams',
        function ($scope, $http, $state, $stateParams) {

            $scope.cat = {};
            $scope.categories = [];
            $scope.cat.id = $stateParams.catId;


            $scope.refreshCategories = function () {
                $http.get('/api/categories').
                    success(function (data, status, headers, config) {
                        $scope.categories = [{id: '', parent: null, name: 'None', text: 'None'}];
                        for (var i in data) {
                            $scope.categories.push({
                                id: data[i]._id,
                                text: $scope.getRootCategory(data[i].name, data),
                                name: data[i].name,
                                parent: data[i].parent
                            });
                        }
                        $scope.updateSelectedCategoryById();
                    })
                    .error(function (data, status, headers, config) {
                    });
            }();

            $scope.update = function (cont) {
                $http.put('/api/categories', {
                    id: $scope.cat.id,
                    name: $scope.cat.name,
                    parent: $scope.cat.parent ? $scope.cat.parent : null
                })
                    .success(function (data, status, headers, config) {
                        if (cont) {
                            $state.go('Category.Add');
                        } else {
                            $state.go('Category.List');
                        }
                    })
                    .error(function (data, status, headers, config) {
                        if(status==404) window.location = '/admin/login';
                    });
            }

            $scope.goBack = function () {
                $state.go('Category.List');
            }

            $scope.getRootCategory = function (categoryName, data) {
                function getParent(name) {
                    for (var i in data) {
                        if (data[i].name == name) {
                            if (!data[i].parent) {
                                return data[i].name;
                            } else {
                                return getParent(data[i].parent) + ' >> ' + name;
                            }
                        } else if (data[i]._id == name) {
                            if (!data[i].parent) {
                                return data[i].name;
                            } else {
                                return getParent(data[i].parent) + ' >> ' + data[i].name;
                            }
                        }
                    }
                }

                return getParent(categoryName);
            };

            $scope.updateSelectedCategoryById = function () {
                for (var i in $scope.categories) {
                    if ($scope.categories[i].id == $scope.cat.id) {
                        $scope.cat.name = $scope.categories[i].name;
                        $scope.cat.parent = $scope.categories[i].parent?$scope.categories[i].parent: '';
                        break;
                    }
                }
            }


        }]);
