'use strict';

angular.module('appAdmin')
    .controller('listCategoryController', ['$scope', '$http', '$state',
        function ($scope, $http, $state) {


            //$scope.cat = {};
            $scope.categories = [];

            $scope.refreshCategories = function () {
                $http.get('/api/categories').
                    success(function (data, status, headers, config) {
                        $scope.categories = [];
                        for (var i in data) {
                            $scope.categories.push({
                                id: data[i]._id,
                                sn: parseInt(i)+1,
                                text: $scope.getRootCategory(data[i].name, data),
                                name: data[i].name,
                                parent: data[i].parent
                            });
                        }
                        //$scope.cat.parent = '';
                    })
                    .error(function (data, status, headers, config) {
                    });
            }
            $scope.refreshCategories();

            $scope.delete = function (id) {

                $http.delete('/api/categories/'+id)
                    .success(function(data, status, headers, config){
                        $scope.refreshCategories();
                    })
                    .error(function(data, status, headers, config){
                        if(status==404) window.location = '/admin/login';
                    });

            }

            $scope.getRootCategory = function (categoryName, data) {
                function getParent(name) {
                    for (var i in data) {
                        if (data[i].name == name) {
                            if (!data[i].parent) {
                                return data[i].name;
                            } else {
                                return getParent(data[i].parent) + ' >> ' + name;
                            }
                        }else if(data[i]._id == name){
                            if (!data[i].parent) {
                                return data[i].name;
                            } else {
                                return getParent(data[i].parent) + ' >> ' + data[i].name;
                            }
                        }
                    }
                }
                return getParent(categoryName);
            };


        }]);
