'use strict';

angular.module('appAdmin')
    .controller('addProductController', ['$scope', '$http', '$state',
        function ($scope, $http, $state) {
            $scope.product = {};
            $scope.categories = [];


            $scope.refreshCategories = function () {
                $http.get('/api/categories').
                    success(function (data, status, headers, config) {
                        $scope.categories = [{id: '', parent: null, name: 'None', text: 'None'}];
                        for (var i in data) {
                            $scope.categories.push({
                                id: data[i]._id,
                                text: $scope.getRootCategory(data[i].name, data),
                                name: data[i].name,
                                parent: data[i].parent
                            });
                        }
                        $scope.product.category = '';
                    })
                    .error(function (data, status, headers, config) {
                    });
            };
            $scope.refreshCategories();

            $scope.save = function (cont) {
                console.log($scope.cat);
                $http.post('/api/products', {
                    name: $scope.product.name,
                    category: $scope.product.category,
                    description: $scope.product.description,
                    price: $scope.product.price,
                    inStock: $scope.product.inStock,
                    discount: $scope.product.discount,
                    sku: $scope.product.sku
                })
                    .success(function (data, status, headers, config) {
                        if (cont) {
                            //$scope.refreshCategories();
                            $scope.refreshProducts();
                        } else {
                            $state.go('Product.List');
                        }
                    })
                    .error(function (data, status, headers, config) {
                    });
            }

            $scope.goBack = function () {
                $state.go('Product.List');
            }

            $scope.refreshProducts = function(){
                $scope.product.name = '',
                $scope.product.category = '',
                $scope.product.description = '',
                $scope.product.price = '',
                $scope.product.inStock = '',
                $scope.product.discount = '',
                $scope.product.sku = ''
            }

            $scope.getRootCategory = function (categoryName, data) {
                function getParent(name) {
                    for (var i in data) {
                        if (data[i].name == name) {
                            if (!data[i].parent) {
                                return data[i].name;
                            } else {
                                return getParent(data[i].parent) + ' >> ' + name;
                            }
                        } else if (data[i]._id == name) {
                            if (!data[i].parent) {
                                return data[i].name;
                            } else {
                                return getParent(data[i].parent) + ' >> ' + data[i].name;
                            }
                        }
                    }
                }

                return getParent(categoryName);
            };




        }]);
