'use strict';

angular.module('appAdmin')
    .controller('listProductController', ['$scope', '$http', '$state',
        function ($scope, $http, $state) {

            $scope.products = [];
            $scope.categories = [];



            $scope.getProducts = function () {
                $http.get('/api/products').
                    success(function (data, status, headers, config) {
                        $scope.products = [];

                        for (var i in data) {
                            $scope.products.push({
                                id: data[i]._id,
                                sn: parseInt(i)+1,
                                name: data[i].name,
                                category:  data[i].category,
                                images: data[i].images,
                                inStock: data[i].inStock,
                                price: data[i].price,
                                discount: data[i].discount,
                                sku: data[i].sku
                            });
                        }

                    })
                    .error(function (data, status, headers, config) {
                    });
            }
            $scope.getProducts();

            $scope.delete = function (id) {

                $http.delete('/api/products/'+id)
                    .success(function(data, status, headers, config){
                        $scope.getProducts();
                    })
                    .error(function(data, status, headers, config){
                        if(status==404) window.location = '/admin/login';
                    });

            }


        }]);
