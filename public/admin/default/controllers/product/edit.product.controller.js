'use strict';

angular.module('appAdmin')
    .controller('editProductController', ['$scope', '$http', '$state', '$stateParams', 'Upload',
        function ($scope, $http, $state, $stateParams, Upload) {
            $scope.product = {};
            $scope.product.id = $stateParams.productId;

            $scope.refreshCategories = function () {
                $http.get('/api/categories')
                    .success(function (data, status, headers, config) {
                        $scope.categories = [{id: '', parent: null, name: 'None', text: 'None'}];
                        for (var i in data) {
                            $scope.categories.push({
                                id: data[i]._id,
                                text: $scope.getRootCategory(data[i].name, data),
                                name: data[i].name,
                                parent: data[i].parent
                            });
                        }
                        $scope.updateSelectedProductById();
                    })
                    .error(function (data, status, headers, config) {
                    });
            };
            $scope.refreshCategories();

            $scope.updateSelectedProductById = function () {
                $http.get('/api/products/' + $scope.product.id)
                    .success(function (data, status, headers, config) {

                        $scope.product.name = data.name;
                        $scope.product.category = data.category;
                        $scope.product.description = data.description;
                        $scope.product.price = data.price;
                        $scope.product.inStock = data.inStock;
                        $scope.product.discount = data.discount;
                        $scope.product.sku = data.sku;
                        if (!$scope.product.category) {
                            $scope.product.category = '';
                        }
                        $scope.product.images =data.images;
                        $scope.product.defaultImage = data.defaultImage;
                    })
                    .error(function (data, status, headers, config) {
                    });

            }

            $scope.update = function (cont) {
                $http.put('/api/products', {
                    id: $scope.product.id,
                    name: $scope.product.name,
                    category: $scope.product.category,
                    description: $scope.product.description,
                    price: parseInt($scope.product.price) ? parseInt($scope.product.price) : 0,
                    inStock: parseInt($scope.product.inStock) ? parseInt($scope.product.inStock) : 0,
                    discount: parseInt($scope.product.discount) ? parseInt($scope.product.discount) : 0,
                    sku: $scope.product.sku,
                    images: $scope.product.images,
                    defaultImage: parseInt($scope.product.defaultImage) ? parseInt($scope.product.defaultImage) : 0
                })
                    .success(function (data, status, headers, config) {
                        if (cont) {
                            $state.go('Product.Edit',{productId:$scope.product.id});
                        } else {
                            $state.go('Product.List');
                        }
                    })
                    .error(function (data, status, headers, config) {
                        if(status==404) window.location = '/admin/login';
                    });
            }

            $scope.goBack = function () {
                $state.go('Product.List');
            }

            $scope.getRootCategory = function (categoryName, data) {
                function getParent(name) {
                    for (var i in data) {
                        if (data[i].name == name) {
                            if (!data[i].parent) {
                                return data[i].name;
                            } else {
                                return getParent(data[i].parent) + ' >> ' + name;
                            }
                        } else if (data[i]._id == name) {
                            if (!data[i].parent) {
                                return data[i].name;
                            } else {
                                return getParent(data[i].parent) + ' >> ' + data[i].name;
                            }
                        }
                    }
                }

                return getParent(categoryName);
            };

            /********************* File Upload ********************/


            $scope.showImage = false;
            $scope.product.images = [];
            $scope.picture = {};
            $scope.picture.files = [];
            //$scope.picture.files[0] = true;
            console.log($scope.picture.files[0]);




            $scope.upload = function(selectedFiles){
                console.log('upload function');

                var file = selectedFiles? selectedFiles[0] : null;
                console.log(selectedFiles);
                Upload.upload({
                    url: '/api/products/media',
                    fields: {
                        name: file.name
                    },
                    file: selectedFiles[0]
                }).progress(function (evt) {
                    console.log('some progress');
                    var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                    $scope.log = 'progress: ' + progressPercentage + '% ' +
                    evt.config.file.name + '\n' + $scope.log;
                }).success(function (data, status, headers, config) {
                    if(status == 500){
                        console.log('upload error');
                    }else if(status == 404){
                        window.location = '/admin/login';
                    }else if(status == 200){
                        var len = $scope.product.images.length;
                        $scope.product.images[len] = {};
                        $scope.product.images[len].id = data._id;
                        $scope.product.images[len].displayOrder = len;
                        $scope.product.images[len].alt = $scope.picture.alt;
                        $scope.product.images[len].title = $scope.picture.title;

                        $scope.picture.files[0]  = null;

                        console.log($scope.product);
                        $scope.update(true);

                    }
                    $scope.log = 'file ' + config.file.name + 'uploaded. Response: ' + JSON.stringify(data) + '\n' + $scope.log;
                });
            }

            $scope.deleteImage = function(imageId){
                $http.delete('/api/products/'+imageId)
                    .success(function(data, status, headers, config){
                        $scope.removeImageFromArray(imageId);
                        $scope.update(true);
                    })
                    .error(function(data, status, headers, config){

                    });
            }

            $scope.removeImageFromArray = function(imageId){
                for(var i in  $scope.product.images){
                    if($scope.product.images[i].id == imageId){
                        $scope.product.images.splice(i,1);
                    }
                }
            }



        }]);
