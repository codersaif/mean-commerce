'use strict';

angular.module('appAdmin')
    .config(function ($stateProvider, $urlRouterProvider, configurationProvider) {
        //
        var VIEWS_DIR = configurationProvider.$get().VIEWS_DIR+'order/';

        // Now set up the states
        $stateProvider
            .state('Order', {
                abstract: true,
                url: '/Order',
                template: '<ui-view/>'
            })
            .state('Order.Add', {
                url: '/add',
                templateUrl: VIEWS_DIR +'add-order.html'
            })
            .state('Order.Edit', {
                url: '/edit',
                templateUrl: VIEWS_DIR +'edit-order.html'
            })
            .state('Order.List', {
                url: '/list',
                templateUrl: VIEWS_DIR +'list-order.html'
            });
    });