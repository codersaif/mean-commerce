'use strict';

angular.module('appAdmin')
    .config(function ($stateProvider, $urlRouterProvider, configurationProvider) {
        //
        var TEMPLATE_DIR = configurationProvider.$get().VIEWS_DIR+'manufacturer/';

        // Now set up the states
        $stateProvider
            .state('Manufacturer', {
                abstract: true,
                url: '/manufacturer',
                template: '<ui-view/>'
            })
            .state('Manufacturer.Add', {
                url: '/add',
                templateUrl: TEMPLATE_DIR +'add-manufacturer.html',
                controller: 'addManufacturerController'
            })
            .state('Manufacturer.Edit', {
                url: '/edit/:mfId',
                templateUrl: TEMPLATE_DIR +'edit-manufacturer.html',
                controller: 'editManufacturerController'
            })
            .state('Manufacturer.List', {
                url: '/list',
                templateUrl: TEMPLATE_DIR +'list-manufacturer.html',
                controller: 'listManufacturerController'
            });
    });