'use strict';

angular.module('appAdmin')
    .config(function ($stateProvider, $urlRouterProvider, configurationProvider) {
        //
        var VIEWS_DIR = configurationProvider.$get().VIEWS_DIR;

        // For any unmatched url, redirect to /dashboard
        $urlRouterProvider.otherwise('dashboard');

        // Now set up the states
        $stateProvider
            .state('dashboard', {
                url: '/dashboard',
                templateUrl: VIEWS_DIR + 'dashboard.html',
                controller: function($scope){
                    $scope.things = ['A', 'Set', 'Of', 'Things'];
                    console.log($scope.things);
                }
            });/*
            .state('categories', {
                url: '/categories',
                templateUrl: VIEWS_DIR +'categories.html'
            })
            .state('categories.add', {
                url: '/add',
                templateUrl: VIEWS_DIR +'add.html'
            })
            .state('categories.edit', {
                url: '/edit',
                templateUrl: VIEWS_DIR +'edit.html'
            })
            .state('categories.list', {
                url: '/list',
                templateUrl: VIEWS_DIR +'list.html'
            })
            .state('categories.tree', {
                url: '/tree',
                templateUrl: 'tree.html'
            })
            .state('state1.list', {
                url: '/list',
                templateUrl: 'partials/state1.list.html',
                controller: function ($scope) {
                    $scope.items = ['A', 'List', 'Of', 'Items'];
                }
            })
            .state('state2', {
                url: '/state2',
                templateUrl: 'partials/state2.html'
            })
            .state('state2.list', {
                url: '/list',
                templateUrl: 'partials/state2.list.html',
                controller: function ($scope) {
                    $scope.things = ['A', 'Set', 'Of', 'Things'];
                }
            });*/
    });