'use strict';

angular.module('appAdmin')
    .config(function ($stateProvider, $urlRouterProvider, configurationProvider) {
        //
        var TEMPLATE_DIR = configurationProvider.$get().VIEWS_DIR+'category/';

        // Now set up the states
        $stateProvider
            .state('Category', {
                abstract: true,
                url: '/category',
                template: '<ui-view/>'
            })
            .state('Category.Add', {
                url: '/add',
                templateUrl: TEMPLATE_DIR +'add-category.html',
                controller: 'addCategoryController'
            })
            .state('Category.Edit', {
                url: '/edit/:catId',
                templateUrl: TEMPLATE_DIR +'edit-category.html',
                controller: 'editCategoryController'
            })
            .state('Category.List', {
                url: '/list',
                templateUrl: TEMPLATE_DIR +'list-category.html',
                controller: 'listCategoryController'
            })
            .state('Category.Tree', {
                url: '/tree',
                templateUrl: TEMPLATE_DIR + 'tree-category.html'
            });
    });