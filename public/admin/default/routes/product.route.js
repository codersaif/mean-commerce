'use strict';

angular.module('appAdmin')
    .config(function ($stateProvider, $urlRouterProvider, configurationProvider) {
        //
        var TEMPLATE_DIR = configurationProvider.$get().VIEWS_DIR+'product/';

        // Now set up the states
        $stateProvider
            .state('Product', {
                abstract: true,
                url: '/Product',
                template: '<ui-view/>'
            })
            .state('Product.Add', {
                url: '/add',
                templateUrl: TEMPLATE_DIR +'add-product.html',
                controller: 'addProductController'
            })
            .state('Product.Edit', {
                url: '/edit/:productId',
                templateUrl: TEMPLATE_DIR +'edit-product.html',
                controller: 'editProductController'
            })
            .state('Product.List', {
                url: '/list',
                templateUrl: TEMPLATE_DIR +'list-product.html',
                controller: 'listProductController'
            });
    });