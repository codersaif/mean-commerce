'use strict';

angular.module('appAdmin')
    .config(function ($stateProvider, $urlRouterProvider, configurationProvider) {
        //
        var TEMPLATE_DIR = configurationProvider.$get().VIEWS_DIR+'setting/';

        // Now set up the states
        $stateProvider
            .state('Setting', {
                abstract: true,
                url: '/setting',
                template: '<ui-view/>'
            })
            .state('Setting.Theme', {
                url: '/theme',
                templateUrl: TEMPLATE_DIR +'theme-setting.html',
                controller: 'themeSettingController'
            });
    });