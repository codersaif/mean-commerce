'use strict';
angular.module('appAdmin')
    .directive('navigationBars',['configuration', function(configuration){
        return {
            restrict: 'A',
            templateUrl: configuration.VIEWS_DIR+'navigation/navigation-bars.html',
            link: function link($scope, elem, attrs) {
            }
        };
    }]);

