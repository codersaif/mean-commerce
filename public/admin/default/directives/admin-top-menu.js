'use strict';
angular.module('appAdmin')
    .directive('adminTopMenu',['configuration', function(configuration){
        return {
            restrict: 'A',
            replace: true,
            templateUrl: configuration.VIEWS_DIR+'navigation/admin-top-menu.html',
            link: function link($scope, elem, attrs) {
            }
        };
    }]);

