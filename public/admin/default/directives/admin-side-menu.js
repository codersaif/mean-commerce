'use strict';
angular.module('appAdmin')
.directive('adminSideMenu',['configuration', function(configuration){
        return {
            restrict: 'A',
            templateUrl: configuration.VIEWS_DIR+'navigation/admin-side-menu.html',
            link: function link($scope, elem, attrs) {
                $('#side-menu').metisMenu();
            }
        };
    }]);

