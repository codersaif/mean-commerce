'use strict';

angular.module('appWeb')
    .service('configuration', function(){
        this.THEME_DIR =  '/web/default/';
        this.VIEWS_DIR = this.THEME_DIR + 'views/'
    });

