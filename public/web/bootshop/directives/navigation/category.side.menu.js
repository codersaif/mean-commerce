'use strict';

angular.module('appWeb')
    .directive('categorySideMenu', ['configuration', '$http', function (configuration, $http) {
        return {
            restrict: 'A',
            replace: true,
            templateUrl: configuration.VIEWS_DIR + 'navigation/category-side-menu.html',

            controller: function ($scope, $http) {

            },
            link: function link(scope, element, attrs) {
                var sideMenu = '';
                $http.get('/api/categories')
                    .success(function (data, status) {
                        for (var i = data.length - 1; i >= 0; i--) {
                            var childFound = false, parentFound = false;

                            for (var j = data.length - 1; j >= 0; j--) {
                                if (data[i]._id == data[j].parent) {
                                    childFound = true;
                                    break;
                                }
                            }
                            if (!childFound) {
                                for (var j = data.length - 1; j >= 0; j--) {
                                    if (data[i].parent == data[j]._id) {
                                        parentFound = true;
                                        break;
                                    }
                                }
                                if (parentFound) {
                                    if (!data[j].sub) {
                                        data[j].sub = [];
                                    }
                                    var len = data[j].sub.length;
                                    data[j].sub.push(data[i]);
                                    data.splice(i, 1);
                                }
                            } else {
                                data[i].parent = null;
                            }
                        }
                        scope.buildSideMenu(data);
                        element.html(sideMenu);
                        scope.enableSideMenu();
                    })
                    .error(function (data, status) {

                    });
                scope.buildSideMenu = function (data) {
                    sideMenu = '<ul id="sideManu" class="nav nav-tabs nav-stacked">';
                    for (var i in data) {
                        var subMenu = data[i].sub ? 'subMenu' : '';
                        var open = i == 0 ? 'open' : '';
                        // adding main menu
                        sideMenu += '<li class="' + subMenu + ' ' + open + '" >';
                        // adding sub menu
                        if (data[i].sub) {
                            sideMenu += '<a href="#">';
                        } else {
                            sideMenu += '<a href="products.html">';
                        }
                        sideMenu += data[i].name + '</a>';
                        if (data[i].sub) {
                            if (!i) {
                                sideMenu += '<ul>';
                            } else {
                                sideMenu += '<ul style="display:none">';
                            }
                            for (var j in data[i].sub) {
                                sideMenu += '<li><a href="products.html"><i class="icon-chevron-right"></i>' + data[i].sub[j].name + '</a></li>';
                            }
                            sideMenu += '</ul>';
                        }
                        sideMenu += '</li>';

                    }
                    sideMenu += '</ul>';
                    console.log(sideMenu);
                }
                scope.enableSideMenu = function () {
                    $('.subMenu > a').click(function (e) {
                        e.preventDefault();
                        var subMenu = $(this).siblings('ul');
                        var li = $(this).parents('li');
                        var subMenus = $('#sidebar li.subMenu ul');
                        var subMenus_parents = $('#sidebar li.subMenu');
                        if (li.hasClass('open')) {
                            if (($(window).width() > 768) || ($(window).width() < 479)) {
                                subMenu.slideUp();
                            } else {
                                subMenu.fadeOut(250);
                            }
                            li.removeClass('open');
                        } else {
                            if (($(window).width() > 768) || ($(window).width() < 479)) {
                                subMenus.slideUp();
                                subMenu.slideDown();
                            } else {
                                subMenus.fadeOut(250);
                                subMenu.fadeIn(250);
                            }
                            subMenus_parents.removeClass('open');
                            li.addClass('open');
                        }
                    });
                    var ul = $('#sidebar > ul');
                    $('#sidebar > a').click(function (e) {
                        e.preventDefault();
                        var sidebar = $('#sidebar');
                        if (sidebar.hasClass('open')) {
                            sidebar.removeClass('open');
                            ul.slideUp(250);
                        } else {
                            sidebar.addClass('open');
                            ul.slideDown(250);
                        }
                    });
                }
            }
        };
    }]);

