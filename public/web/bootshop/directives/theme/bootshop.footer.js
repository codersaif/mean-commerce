'use strict';
angular.module('appWeb')
    .directive('bootshopFooter',['configuration', function(configuration){
        return {
            restrict: 'A',
            templateUrl: configuration.VIEWS_DIR+'theme/bootshop-footer.html',
            link: function link($scope, elem, attrs) {
            }
        };
    }]);

