'use strict';
angular.module('appWeb')
   .directive('bootshopHeader',['configuration', function(configuration){
        return {
            restrict: 'A',
            templateUrl: configuration.VIEWS_DIR+'theme/bootshop-header.html',
            link: function link($scope, elem, attrs) {


            }
        };
    }]);

