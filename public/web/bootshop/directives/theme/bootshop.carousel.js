'use strict';
angular.module('appWeb')
    .directive('bootshopCarousel',['configuration', function(configuration){
        return {
            restrict: 'A',
            templateUrl: configuration.VIEWS_DIR+'theme/bootshop-carousel.html',
            link: function link($scope, elem, attrs) {
            }
        };
    }]);

