'use strict';

angular.module('appWeb')
    .provider('configuration', function () {
        return {
            $get: function () {
                return {
                    THEME_DIR: '/web/bootshop/',
                    VIEWS_DIR: '/web/bootshop/views/'
                }
            }
        }
    }
);