'use strict';

exports.user = function (req, res, next) {
    if (req.session.user) {
        next();
    } else {
        res.redirect('/login');
    }
}


exports.admin = function (req, res, next) {

    if (req.session.admin) {
        return next();
    } else {
        req.session.destroy(function () {
            res.redirect('/admin/login');
        });
    }
}