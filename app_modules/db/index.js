'use strict';

var db = function(req, res, next){
    mongoose.connect('mongodb://localhost/myshop');
    next();
}

exports.db = db;