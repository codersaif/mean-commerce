'use strict';
var config = {
    ADMIN_VIEW_DIR: 'admin/',
    ADMIN_THEME: 'default/',

    WEB_VIEW_DIR: 'web/',
    //WEB_THEME: 'default/',
    WEB_THEME: 'bootshop/',


    PUBLIC_ADMIN: '/admin/default',
    //PUBLIC_WEB: '/web/default'
    PUBLIC_WEB: '/web/bootshop'
};


module.exports = config;