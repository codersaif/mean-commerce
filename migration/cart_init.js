var mongoose = require('mongoose');
var db = mongoose.connect('mongodb://localhost/myshop');
var q = require('q');
require('../api/models/cart_model.js');
var Address = mongoose.model('Address');
var Billing = mongoose.model('Billing');
var Product = mongoose.model('Product');
var ProductQuantity = mongoose.model('ProductQuantity');
var Order = mongoose.model('Order');
var Customer = mongoose.model('Customer');

var Category = require('../api/models/categoryModel.js');



function addCategory(parent, name) {
    var deferred = q.defer();
    var category = new Category({
        name: name,
        parent: parent
    });
    category.save(function (err, cat) {
        if (err) {
            return deferred.reject();
        } else {
            return deferred.resolve();
        }
    });
    return deferred.promise;
}
function addProduct(customer, order, name, category, imageFile,
                    price, description, inStock) {
    var product = new Product({
        name: name, imageFile: imageFile,
        price: price, category: category, description: description,
        inStock: inStock
    });
    product.save(function (err, results) {
        order.items.push(new ProductQuantity({
            quantity: 1,
            product: [product]
        }));
        order.save();
        customer.save();
        console.log("Product " + name + " Saved.");
    });
}
Product.remove().exec(function () {
    Order.remove().exec(function () {
        Customer.remove().exec(function () {
            var shipping = new Address({
                name: 'Customer A',
                address: 'Somewhere',
                city: 'My Town',
                state: 'CA',
                zip: '55555'
            });
            var billing = new Billing({
                cardType: 'Visa',
                name: 'Customer A',
                number: '1234567890',
                expireMonth: 1,
                expireYear: 2020,
                address: shipping
            });
            var customer = new Customer({
                userId: 'customerA',
                shipping: shipping,
                billing: billing,
                cart: []
            });
            var order = new Order({
                userId: customer.userId,
                items: [],
                shipping: customer.shipping,
                billing: customer.billing
            });
            customer.save(function (err, result) {
                Category.remove().exec(function (err, result) {
                    var q = require('q');
                    q.fcall(addCategory("", "Electronics"))
                        .then(addCategory("Electronics", "Camera"))
                        .then(addCategory("Electronics", "Computer"))
                        .then(addCategory("Computer", "Accessories"))
                        .then(addCategory("Computer", "Desktop"))
                        .then(addCategory("Computer", "Laptop"))
                        .then(addCategory("Computer", "Software & Games"))
                        .then(addCategory("Electronics", "Cell Phone"))
                        .then(addCategory("Cell Phone", "IPhone"))
                        .then(addCategory("Cell Phone", "Android"))
                        .then(addCategory("", "Apparel & Shoes"))
                        .then(addCategory("", "Digital Download"))
                        .then(addCategory("", "Jewelery"))
                        .then(addCategory("", "Gift Cards"))
                        .then(addCategory("", "Car"))
                        .then(addCategory("Car", "BMW"))
                        .then(addCategory("Car", "Porsche"))
                        .then(addCategory("Car", "Ferrari"))
                        .then(addCategory("Car", "Bugatti"))
                        .then(addCategory("Jewelery", "Ring"))
                        .then(addCategory("Ring", "Diamond Ring"))
                        .then(order.save(function (err, result) {
                            Category.getIdByName('Camera', function (catId) {
                                addProduct(customer, order, 'Red Flower Print', catId,
                                    'Chrysanthemum.jpg', 12.34,
                                    'A close look of Chrysanthemum flower',
                                    Math.floor((Math.random() * 10) + 1));
                            });
                            Category.getIdByName('Camera', function (catId) {
                                addProduct(customer, order, 'Desert Print', catId,
                                    'Desert.jpg', 45.45,
                                    'View of a desert in USA',
                                    Math.floor((Math.random() * 10) + 1));
                            });
                            Category.getIdByName('Camera', function (catId) {
                                addProduct(customer, order, 'Hydrangeas Print', catId,
                                    'Hydrangeas.jpg', 38.52,
                                    'Look at the amazing white flower.',
                                    Math.floor((Math.random() * 10) + 1));
                            });
                            Category.getIdByName('Camera', function (catId) {
                                addProduct(customer, order, 'Jellyfish Under Water Print', catId,
                                    'Jellyfish.jpg', 77.45,
                                    'Jellyfish swimming under water',
                                    Math.floor((Math.random() * 10) + 1));
                            });
                            Category.getIdByName('Camera', function (catId) {
                                addProduct(customer, order, 'Lighthouse Print', catId,
                                    'Lighthouse.jpg', 45.45,
                                    'View of a Lighthouse',
                                    Math.floor((Math.random() * 10) + 1));
                            });
                            Category.getIdByName('Camera', function (catId) {
                                addProduct(customer, order, 'Penguins Print', catId,
                                    'Penguins.jpg', 38.52,
                                    'Three penguin talking each other.',
                                    Math.floor((Math.random() * 10) + 1));
                            });
                            Category.getIdByName('Camera', function (catId) {
                                addProduct(customer, order, 'Tulip flowers Print', catId,
                                    'Tulips.jpg', 77.45,
                                    'Tulip flowers in the garden',
                                    Math.floor((Math.random() * 10) + 1));
                            });
                        }))
                        .catch(function (error) {
                            // Handle any error from all above steps
                        })
                        .done();
                    //addCategory("", "Electronics")
                    //addCategory("Electronics", "Camera");

                });


            });
        });
    });
});